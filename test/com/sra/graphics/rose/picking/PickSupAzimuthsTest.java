/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;


import java.awt.Graphics2D;

import com.sra.graphics.rose.picking.PickSupAzimuths;
import com.sra.graphics.rose.picking.PickedOffsets;
import edu.mines.jtk.mosaic.Transcaler;
import junit.framework.TestCase;
import org.junit.Assert;

public class PickSupAzimuthsTest extends TestCase {
  private PickSupAzimuths _pa;

   @Override
   public void setUp() {
     _pa = new PickSupAzimuths(36, 10, new PickedOffsets.PaintHighlight() {
       @Override
       public void paint(Graphics2D g2d, Transcaler ts, double begin, double end) {
       }
     });
   }

   @Override
   public void tearDown() {
     for (int i = 0; i < 36; i++) {
       System.out.println(i + " " + _pa.isPicked(i * 10));
     }
   }

   public void test() {
     _pa.pick(null, null, 20);
     _pa.pick(null, null, 0);

     Assert.assertTrue(_pa.isPicked(20));
     Assert.assertTrue(_pa.isPicked(10));
     Assert.assertTrue(_pa.isPicked(0));

     Assert.assertTrue(_pa.isPicked(180));
     Assert.assertTrue(_pa.isPicked(190));
     Assert.assertTrue(_pa.isPicked(200));

     Assert.assertTrue(!_pa.isPicked(210));
     Assert.assertTrue(!_pa.isPicked(170));
     Assert.assertTrue(!_pa.isPicked(30));
     Assert.assertTrue(!_pa.isPicked(350));
   }
}
