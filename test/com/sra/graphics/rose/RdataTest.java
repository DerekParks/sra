/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose;

import junit.framework.TestCase;
import org.junit.Assert;

public class RdataTest extends TestCase {
  
  public void test1() {
    Rdata rd = new Rdata(1, 1, new float[] {100}, new float[] {0}) ;

    Assert.assertEquals(0, rd.getAngle(0), .01);
    Assert.assertEquals(100f, rd.getMaxOffset(), .001f);
    Assert.assertEquals(100f, rd.getOffsetIncrement(), .001f);
    Assert.assertEquals(360f, rd.getSectorIncrement(), .001f);
    Assert.assertEquals(1, rd.getSectors());
    Assert.assertEquals(1, rd.getRings());

    Assert.assertEquals(1, rd.getHitCount(100f, 0f));
    Assert.assertEquals(1, rd.getHitCount(100f, 90f));
    Assert.assertEquals(1, rd.getHitCount(100f, 180f));
    Assert.assertEquals(1, rd.getHitCount(100f, 270f));
    Assert.assertEquals(1, rd.getHitCount(50f, 0f));
    Assert.assertEquals(1, rd.getHitCount(0.1f, 0f));
    Assert.assertEquals(1, rd.getHitCount(0f, 0f));
  }

  public void test2() {
    Rdata rd = new Rdata(2, 1, new float[] {100}, new float[] {0}) ;
    Assert.assertEquals(1, rd.getHitCount(100f, 0f));
    Assert.assertEquals(1, rd.getHitCount(100f, 90f));
    Assert.assertEquals(1, rd.getHitCount(100f, 180f));
    Assert.assertEquals(1, rd.getHitCount(100f, 270f));
    Assert.assertEquals(0, rd.getHitCount(50f, 0f));
    Assert.assertEquals(0, rd.getHitCount(0.1f, 0f));
    Assert.assertEquals(0, rd.getHitCount(0f, 0f));
  }

  public void test3() {
    Rdata rd = new Rdata(2, 1, new float[] {50, 100}, new float[] {0,0}) ;
    
    Assert.assertEquals(0, rd.getAngle(0), .01);
    Assert.assertEquals(100f, rd.getMaxOffset(), .001f);
    Assert.assertEquals(50, rd.getOffsetIncrement(), .001f);
    Assert.assertEquals(360f, rd.getSectorIncrement(), .001f);
    Assert.assertEquals(1, rd.getSectors());
    Assert.assertEquals(2, rd.getRings());

    Assert.assertEquals(1, rd.getHitCount(100f, 0f));
    Assert.assertEquals(1, rd.getHitCount(100f, 90f));
    Assert.assertEquals(1, rd.getHitCount(100f, 180f));
    Assert.assertEquals(1, rd.getHitCount(100f, 270f));
    Assert.assertEquals(1, rd.getHitCount(50f, 0f));
    Assert.assertEquals(1, rd.getHitCount(0.1f, 0f));
    Assert.assertEquals(1, rd.getHitCount(0f, 0f));
  }

  public void test4() {
    Rdata rd = new Rdata(2, 1, new float[] {10, 50, 100}, new float[] {0,0,0}) ;

    Assert.assertEquals(1, rd.getHitCount(100f, 0f));
    Assert.assertEquals(1, rd.getHitCount(100f, 90f));
    Assert.assertEquals(1, rd.getHitCount(100f, 180f));
    Assert.assertEquals(1, rd.getHitCount(100f, 270f));
    Assert.assertEquals(2, rd.getHitCount(50f, 0f));
    Assert.assertEquals(2, rd.getHitCount(0.1f, 0f));
    Assert.assertEquals(2, rd.getHitCount(0f, 0f));
  }

  public void test5() {
    Rdata rd = new Rdata(1, 2, new float[] {100}, new float[] {0}) ;

    Assert.assertEquals(0f, rd.getAngle(0), .01);
    Assert.assertEquals(180f, rd.getAngle(1), .01);
    Assert.assertEquals(100f, rd.getMaxOffset(), .001f);
    Assert.assertEquals(100f, rd.getOffsetIncrement(), .001f);
    Assert.assertEquals(180, rd.getSectorIncrement(), .001f);
    Assert.assertEquals(2, rd.getSectors());
    Assert.assertEquals(1, rd.getRings());
    
    Assert.assertEquals(1, rd.getHitCount(100f, 0f));
    Assert.assertEquals(1, rd.getHitCount(100f, 90f));
    Assert.assertEquals(1, rd.getHitCount(100f, 180));
    Assert.assertEquals(0, rd.getHitCount(100f, 270f));
    Assert.assertEquals(1, rd.getHitCount(50f, 0f));
    Assert.assertEquals(1, rd.getHitCount(0.1f, 0f));
    Assert.assertEquals(1, rd.getHitCount(0f, 0f));
  }

  public void test6() {
    float[] x = new float[] {10, 9, 10, -10};
    float[] y = new float[] {10, 9, -10, -10};
    float[] offset = Rdata.toOffset(x, y);
    float[] angle = Rdata.toAngle(x, y);
    
    Rdata rd = new Rdata(1, 4, offset, angle);
    
    Assert.assertEquals(1, rd.getRings());
    Assert.assertEquals(4, rd.getSectors());
    
    int rings = rd.getRings() ;
    for(int i=0;i<rings;i++) {
      int sectors = rd.getSectors() ;
      for(int j=0;j<sectors;j++) {
        int hc = rd.getHitCount(i, j) ;
//        System.out.println("ring=" + i + " sector = " + j + " hits = " + hc) ;
      }
    }
    
    Assert.assertEquals(2, rd.getHitCount(0, 0));
    Assert.assertEquals(0, rd.getHitCount(0, 1));
    Assert.assertEquals(1, rd.getHitCount(0, 2));
    Assert.assertEquals(1, rd.getHitCount(0, 3));
  } 
  
  public void test7() {
    float[] offset = new float[] {50, 10, 35} ;
    float[] angle = new float[] {180, 45, 45} ;
    
    Rdata rd = new Rdata(2, 4, offset, angle) ;
    
    Assert.assertEquals(50f, rd.getMaxOffset(), .0001f);
    Assert.assertEquals(25f, rd.getOffsetIncrement(), .0001f);
    Assert.assertEquals(2, rd.getRings());
    Assert.assertEquals(4, rd.getSectors());
    
    int rings = rd.getRings() ;
    for(int i=0;i<rings;i++) {
      int sectors = rd.getSectors() ;
      for(int j=0;j<sectors;j++) {
        int hc = rd.getHitCount(i, j) ;
        System.out.println("ring=" + i + " sector = " + j + " hits = " + hc) ;
      }
    }
    
    Assert.assertEquals(1, rd.getHitCount(0, 0));
    Assert.assertEquals(0, rd.getHitCount(0, 1));
    Assert.assertEquals(0, rd.getHitCount(0, 2));
    Assert.assertEquals(0, rd.getHitCount(1, 2));
  }
}
