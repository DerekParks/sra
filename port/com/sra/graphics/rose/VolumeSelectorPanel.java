/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.lgc.prowess.display.seiscomp.AxisLocationKey;
import com.lgc.prowess.display.util.FrameworkAxisPanel;
import com.lgc.prowess.javaseis.smart.SmartFramework;
import com.lgc.prowess.jms.LocationEvent;
import com.lgc.prowess.jms.LocationEventListener;

/**
 * Hypercube, Volume, frame sliders.
 * @author Derek Parks
 */
public class VolumeSelectorPanel extends JPanel implements ChangeListener, LocationEventListener {
  private static final long serialVersionUID = 1L;
  private static final Logger LOG = Logger.getLogger(VolumeSelectorPanel.class.getName());
  private FrameworkAxisPanel _navigationSliders = null;
  private final RoseDiagramPanel _roseDiagramPanel;
  private final SmartFramework sf;
  private final int nDims;
  VolumeSelectorPanel(final RoseDiagramPanel roseDiagramPanel) {
    super(new GridBagLayout());
    _roseDiagramPanel = roseDiagramPanel;

    sf = _roseDiagramPanel.getSmartFramework();
    nDims = sf.getDataDimensions();
    updateSliders();
  }

  public void updateSliders() {

    if(_navigationSliders != null) {
      remove(0);
    }

    if (nDims > 2) {
      GridBagConstraints gbc = new GridBagConstraints();
      // Determine number of sliders.
      int[] dims = new int[_roseDiagramPanel.getShotPerVolume() ? nDims - 3 : nDims - 2];
      for (int iDim = 0; iDim < dims.length; ++iDim) {
        dims[iDim] = nDims - 1 - iDim;
      }

      _navigationSliders = new FrameworkAxisPanel(sf, dims,
          FrameworkAxisPanel.LabelPosition.LEFT);

      _navigationSliders.addChangeListener(this);

      gbc.gridx = -1;
      gbc.gridx++;
      gbc.gridy = 0;
      gbc.anchor = GridBagConstraints.WEST;
      gbc.weightx = 1;
      gbc.fill = GridBagConstraints.HORIZONTAL;

      add(_navigationSliders.getContainer(), gbc);
      _navigationSliders.setCenterLocation();
      revalidate();
      repaint();
    } else {
      _navigationSliders = null;
    }
  }

  @Override
  public void stateChanged(ChangeEvent e) {
    if (_navigationSliders == null && _navigationSliders.isAdjusting()) {
      return;
    }
    final AxisLocationKey[] keys = _navigationSliders.getLocationKeys();


    HashMap<String, Long> hashSet = new HashMap<>();
    hashSet.put("", -1l);
    for(AxisLocationKey key: keys ) {
        hashSet.put(key.getHdrName(), key.getLogicalKey());
    }
    final long hKey = hashSet.get(_roseDiagramPanel.getHyperCubeHeader());
    final long vKey = hashSet.get(_roseDiagramPanel.getVolumeHeader());
    final long fKey = hashSet.get(_roseDiagramPanel.getFrameHeader());
    if(hKey != _roseDiagramPanel.getHyperCubeKey() || vKey != _roseDiagramPanel.getVolumeKey() || fKey != _roseDiagramPanel.getFameKey())  {
      LOG.info("Location from sliders = "
              + Arrays.toString(_navigationSliders.getLocationKeys()));
      _roseDiagramPanel.updateView(hKey, vKey, fKey);


    }
  }

  @Override
  public void receiveLocation(LocationEvent locationEvent) {
    if (_roseDiagramPanel.isEventValidForMe(locationEvent)) {

      long[] newKeys = _roseDiagramPanel.getKeys(locationEvent);

      final AxisLocationKey[] keys = _navigationSliders.getLocationKeys();
      int i = keys.length -1;
      for (int j = 0; j < newKeys.length; j++) {
        long thisKey = newKeys[j];
        if (thisKey < 0) {
          continue;
        }
        keys[i--].setLogicalKey(thisKey);
      }
      SwingUtilities.invokeLater(new Runnable() {
              @Override
              public void run() {
                _navigationSliders.setLocationKeys(keys, false);
              }
            });

    }
  }

}
