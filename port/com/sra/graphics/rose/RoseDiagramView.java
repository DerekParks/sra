/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.logging.Logger;

import edu.mines.jtk.awt.ColorMap;
import edu.mines.jtk.awt.ColorMapped;
import edu.mines.jtk.mosaic.Projector;
import edu.mines.jtk.mosaic.TiledView;
import edu.mines.jtk.mosaic.Transcaler;
import edu.mines.jtk.util.Almost;

/**
 * TiledView implementation for the rose diagram. Most of the drawing is done here.
 * @author Steve Angelovich
 * @author Derek Parks
 *
 */
public class RoseDiagramView extends TiledView implements ColorMapped {
  private static final Logger LOG = Logger.getLogger(RoseDiagramView.class.getName());
  private static final float DEFAULT_OFFSET = 100f; //if there isn't any data to plot, use the offset
  private final static Color _radialAxisColor = Color.BLACK;
  private final static Color _offsetAxisColor = Color.BLACK;
  private final static Color _backGroundColor = Color.WHITE;
  private Rdata _data;
  private final boolean _labelRadialAxis = true;
  private final Stroke _stroke = new BasicStroke(1.0f);
  private final ColorMap _colorMap = new ColorMap(ColorMap.JET);
  private final Orientation _orientation;

  /**
   * Orientation of sample axes x1 and x2. For example, the default
   * orientation X1RIGHT_X2UP corresponds to x1 increasing horizontally
   * from left to right, and x2 increasing vertically from bottom to top.
   */
  public enum Orientation {
    X1RIGHT_X2UP,
    X1DOWN_X2RIGHT
  }

  @SuppressWarnings("unused")
  private RoseDiagramView() {
    _orientation = Orientation.X1RIGHT_X2UP;
  }

  public RoseDiagramView(int rings, int sectors, float[] x1, float[] x2, Orientation orientation) {
    if (!Orientation.X1RIGHT_X2UP.equals(orientation)) {
      throw new IllegalArgumentException("Currently only supports Orientation.X1RIGHT_X2UP");
    }
    _orientation = orientation;
    set(rings, sectors, x1, x2);
  }

  public void set(int rings, int sectors, float[] x1, float[] x2) {
    float[] offset = Rdata.toOffset(x1, x2);
    float[] angle = Rdata.toAngle(x1, x2);
    _data = new Rdata(rings, sectors, offset, angle);

    float maxHitCount = _data.getMaxHitCount();

    if (maxHitCount < 1.f) {
      maxHitCount = 1f;
    }
    _colorMap.setValueRange(1, maxHitCount);

    updateBestProjectors();
    repaint();
  }

  /**
   * Called when we might need realignment.
   */
  private void updateBestProjectors() {

    double u0 = 0.0;
    double u1 = 1.0;
    float offset = getNonZeroMaxOffset();

    if (_labelRadialAxis) {
      u0 = 0.15;
      u1 = 0.85;
    }

    // Best projectors.
    Projector bhp = new Projector(-1f * offset, offset, u0, u1);
    Projector bvp = new Projector(offset, -1f * offset, u0, u1);
    setBestProjectors(bhp, bvp);
  }

  public Transcaler getCombinedTranscaler() {
    Projector hp = getHorizontalProjector();
    Projector vp = getVerticalProjector();

    return getTranscaler().combineWith(hp, vp);
  }


  @Override
  public void paint(Graphics2D g2d) {
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    paintWedges(g2d);
    paintCircle(g2d);

    paintRadialAxis(g2d);
    paintRadialAxisLabel(g2d);
  }

  private void paintWedges(Graphics2D g2d) {
    //origin in world coordinates is always at 0,0
    Transcaler ts = getCombinedTranscaler();

    float deltaOffset = _data.getOffsetIncrement();

    int rings = _data.getRings();
    for (int i = 0; i < rings; i++) {
      float r1 = i * deltaOffset;
      float r2 = (i + 1) * deltaOffset;
      LOG.fine("_maxOffset=" + _data.getMaxOffset() + " deltaOffset=" + deltaOffset + " r1=" + r1 + " r2=" + r2);
      Area a1 = new Area(getCircle(r1, ts));
      Area a2 = new Area(getCircle(r2, ts));
      a2.subtract(a1);
      int sectors = _data.getSectors();
      for (int j = 0; j < sectors; j++) {
        float angle1 = _data.getAngle(j);
        float angle2 = _data.getAngle(j + 1);
        int hitCount = _data.getHitCount(i, j);

        LOG.fine("hitCount = " + hitCount + " ring=" + i + " segment = " + j);
        //If we have no hits no need to draw the sector
        Color c;
        if (hitCount > 0) {
          c = _colorMap.getColor(hitCount);
        } else {
          c = _backGroundColor;
        }
        Area a3 = new Area(a2);
        a3.intersect(new Area(getWedge(r2 * 2, angle1, angle2, ts)));
        g2d.setColor(c);
        g2d.setStroke(_stroke);
        g2d.fill(a3);

      }

    }

  }

  private void paintCircle(Graphics2D g2d) {
    //origin in world coordinates is always at 0,0
    Transcaler ts = getCombinedTranscaler();

    float deltaOffset = _data.getOffsetIncrement();
    g2d.setStroke(_stroke);
    g2d.setColor(_offsetAxisColor);
    int rings = _data.getRings();
    for (int i = 1; i <= rings; i++) {
      float radius = i * deltaOffset;
      LOG.fine("_maxOffset=" + _data.getMaxOffset() + " deltaOffset=" + deltaOffset + " radius=" + radius);
      Shape circle = getCircle(radius, ts);

      g2d.draw(circle);
    }
  }

  private void paintRadialAxis(Graphics2D g2d) {
    Projector hp = getHorizontalProjector();
    Projector vp = getVerticalProjector();
    Transcaler ts = getCombinedTranscaler();

    double x0 = hp.v0();
    double y0 = vp.v0();
    double x1 = hp.v1();
    double y1 = vp.v1();

    float wr = (float) (Math.min((x1 - x0), (y1 - y0)) * .5f);
    LOG.fine("x0=" + x0 + " y0=" + y0 + " x1=" + x1 + " y1=" + y1 + " wr=" + wr);
    g2d.setStroke(_stroke);
    g2d.setColor(_radialAxisColor);

    int sectors = _data.getSectors();
    for (int i = 0; i < sectors; i++) {
      float angle = _data.getAngle(i);
      Shape line = getLine2D(wr, angle, ts);
      g2d.draw(line);
    }

  }

  private float getNonZeroMaxOffset() {
    final float result = _data.getMaxOffset();
    if (Almost.FLOAT.zero(result)) {
      return DEFAULT_OFFSET;
    } else {
      return result;
    }
  }

  public float getSectorIncrement() {
    return _data.getSectorIncrement();
  }

  public int getNSectors() {
    return _data.getSectors();
  }

  public float getOffsetIncrement() {
    return _data.getOffsetIncrement();
  }

  public int getNRings() {
    return _data.getRings();
  }

  private void paintRadialAxisLabel(Graphics2D g2d) {
    Transcaler ts = getCombinedTranscaler();

    //not sure how to scale the fonts in a useful way 
    Font f = g2d.getFont();
    Font scaled = new Font(f.getName(), f.getStyle(), 12);
    g2d.setFont(scaled);

    AffineTransform defaultAt = g2d.getTransform();
    FontMetrics fm = g2d.getFontMetrics();

    double wx0 = 0;
    double wy0 = 0;
    int ux0 = ts.x(wx0);
    int uy0 = ts.y(wy0);

    //Use a '00' to compute a distance in device coordinates which then 
    //is converted to a distance in world coordinates.  This additional
    //offset is used to space the text from the last ring on the graph.
    String zero = "00";
    Rectangle2D bZero = fm.getStringBounds(zero, g2d);
    double u = bZero.getWidth();
    double wgap = ts.width((int) Math.ceil(u));

    float inc = _data.getSectorIncrement();
    int sectors = _data.getSectors();
    float maxOffset = getNonZeroMaxOffset();
    for (int sector = 0; sector < sectors; sector++) {

      float angle = sector * inc;
      float azimuth = 450 - angle;
      if (azimuth < 0) azimuth += 360f;
      if (azimuth >= 360) azimuth -= 360f;

      String str = azimuth + "";

      //Compute the width of the label in world coordinates
      Rectangle2D bounds = fm.getStringBounds(str, g2d);
      double uLabelWidth = bounds.getWidth();
      double wLabelWidth = ts.width((int) Math.ceil(uLabelWidth));

      double wr = maxOffset;
      boolean flip = false;
      if (angle > 90 && angle < 270) {
        flip = true;
        //if we are fliping the text we need to add the width of the text to the 
        //radius so the x y coordinates are computed properly
        wr = wr + wLabelWidth + wgap;
      } else {
        wr = wr + wgap;
      }

      //convert the radius and angle to x and y in world coordinate
      double wx = Math.cos(Math.toRadians(angle)) * wr;
      double wy = Math.sin(Math.toRadians(angle)) * wr;

      //convert to device coordinates
      int ux1 = ts.x(wx0 + wx);
      int uy1 = ts.y(wy0 + wy);
      LOG.fine("angle=" + angle + " xOrigin = " + ux0 + " yOrigin = " + uy0 + " ux1=" + ux1 + " uy1=" + uy1);

      //we recompute the angle because the aspect ratio is not constrained to 1.  The display
      //device may cause the circle in our polar graph to be an ellipse.  We recompute the angle in
      //device coordinates so when we rotate it aligns properly.
      double dux = ux1 - ux0;
      double duy = uy1 - uy0;
      double theta = Math.atan2(duy, dux);
      LOG.fine("angle=" + angle + " theta = " + Math.toDegrees(theta));

      //we use the translate, and rotate to move and rotate the labels to be perpendicular to the graph
      AffineTransform at = g2d.getTransform();

      at.translate(ux1, uy1);
      if (flip) {
        //flip 180 degrees so the labels are not upside down
        at.rotate(theta + Math.PI);
      } else {
        at.rotate(theta);
      }
      g2d.setTransform(at);
      g2d.drawString(str, 0, 0);
      g2d.setTransform(defaultAt);
    }

  }

  @Override
  public ColorMap getColorMap() {
    return _colorMap;
  }

  public static Polygon getWedge(double wr, double angle1, double angle2, Transcaler ts) {

    //origin is 0, 0 in world coordinates
    int ux0 = ts.x(0.);
    int uy0 = ts.y(0.);
    double wx1 = Math.cos(Math.toRadians(angle1)) * wr;
    double wy1 = Math.sin(Math.toRadians(angle1)) * wr;
    int ux1 = ts.x(wx1);
    int uy1 = ts.y(wy1);

    double wx2 = Math.cos(Math.toRadians(angle2)) * wr;
    double wy2 = Math.sin(Math.toRadians(angle2)) * wr;
    int ux2 = ts.x(wx2);
    int uy2 = ts.y(wy2);

    Polygon p = new Polygon(new int[]{ux0, ux1, ux2}, new int[]{uy0, uy1, uy2}, 3);
    return p;
  }

  private static Line2D.Float getLine2D(float wr, float angle, Transcaler ts) {

    //origin is 0, 0 in world coordinates
    int ux0 = ts.x(0.);
    int uy0 = ts.y(0.);

    double wx1 = Math.cos(Math.toRadians(angle)) * wr;
    double wy1 = Math.sin(Math.toRadians(angle)) * wr;

    int ux1 = ts.x(wx1);
    int uy1 = ts.y(wy1);
    LOG.fine("xOrigin = " + ux0 + " yOrigin = " + uy0 + " ux1=" + ux1 + " uy1=" + uy1);
    Line2D.Float line = new Line2D.Float(ux0, uy0, ux1, uy1);
    return line;
  }

  public static Ellipse2D.Float getCircle(double radius, Transcaler ts) {
    if (radius < 0) {
      throw new IllegalArgumentException("Radius must be positive");
    }

    //we want upper left corner which will define a rectangle that we draw into
    double wx0 = 0.0 - radius;
    double wy0 = 0.0 + radius;

    //lower right corner
    double wx1 = 0.0 + radius;
    double wy1 = 0.0 - radius;

    int ux0 = ts.x(wx0);
    int uy0 = ts.y(wy0);

    int ux1 = ts.x(wx1);
    int uy1 = ts.y(wy1);

    int w = ux1 - ux0;
    int h = uy1 - uy0;

    LOG.fine("wx0=" + wx0 + " wy0=" + wy0 + "wx1=" + wx1 + " wy1=" + wy1 + " ux0=" + ux0 + " uy0=" + uy0 + " ux1=" + ux1 + " uy1=" + uy1 + " w=" + w + " h=" + h);
    Ellipse2D.Float circle = new Ellipse2D.Float(ux0, uy0, w, h);
    return circle;
  }
}