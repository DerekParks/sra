/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;


import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import com.sra.graphics.rose.Rdata;
import com.sra.graphics.rose.RoseDiagramView;
import edu.mines.jtk.awt.ModeManager;
import edu.mines.jtk.mosaic.Transcaler;

/**
 * 
 * @author Derek Parks
 *
 */
public class RoseDiagramArbAzimuthPickMode extends AbstractPickMode {
  //private static final Logger LOG = Logger.getLogger(RoseDiagramArbAzimuthPickMode.class.getName());
  private static final long serialVersionUID = 1L;
  private PickArbAzimuths _picked;

  public RoseDiagramArbAzimuthPickMode(final ModeManager modeManager) {
    super(modeManager, "Select Arbitrary Azimuths", "Select arbitrary azimuths and display in new trace display", KeyEvent.VK_C);
  }

  @Override
  protected void beginPanelSelect(MouseEvent e, Transcaler ts) {
    final RoseDiagramView view = getView();
    final double deltaAZ = view.getSectorIncrement();
    final int nSectors = view.getNSectors();
    final double deltaOffset = view.getOffsetIncrement();
    final int rings = view.getNRings();

    if(_picked == null) {
      _picked = new PickArbAzimuths(nSectors, deltaAZ, new PickedOffsets.PaintHighlight() {
        @Override
        public void paint(Graphics2D g2d, Transcaler ts, double begin, double end) {
          RoseDiagramAzimuthPickMode.paintHighlightWedge(g2d, ts, deltaOffset * rings, begin, end);
        }
      });
    }

    if(e.getButton() == MouseEvent.BUTTON1) {
      _picked.pick((Graphics2D) getTile().getGraphics(), ts, Rdata.toAngle(ts.x(getXStart()), ts.y(getYStart())));
    }
  }

  @Override
  protected void endPanelSelect(MouseEvent e, Transcaler ts) {
    if(e.getButton() == MouseEvent.BUTTON3) {
      startTraceDisplay(_picked);
      _picked = null;
    }
  }

  @Override
  protected void duringPanelSelect(MouseEvent e, Transcaler ts) {
    if(e.getButton() == MouseEvent.BUTTON1) {
          _picked.pick((Graphics2D) getTile().getGraphics(), ts, Rdata.toAngle(ts.x(getXLast()), ts.y(getYLast())));
    }
  }
}
