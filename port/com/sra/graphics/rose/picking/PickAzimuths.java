/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;


import java.awt.Graphics2D;

import edu.mines.jtk.mosaic.Transcaler;

/**
 * Picking of continuous azimuths.
 * @author Derek Parks
 *
 */
public class PickAzimuths extends PickedOffsets {
  public PickAzimuths(int nOffsets, double offsetInc, PaintHighlight paint) {
    super(nOffsets, offsetInc, paint);
    setEndIndex(-1);
    setStartIndex(-1);
  }


  private int distAntiClock(int i1, int i2) {
    if(i2 < i1) {
      return getN()-i1 + i2;
    } else {
      return i2 - i1;
    }

  }

  private int distClock(int i1, int i2) {
    if(i2 < i1) {
      return i1 - i2;
    } else {
      return i1 + getN() - i2;
    }
  }

  private boolean isBetweenClockwise(int index, int start, int end) {

    if(end > start) {
      return (index <= start && index >= 0) ||  (index >= end && index <= getN());
    } else {
      return index <= start && index >= end;
    }


  }

  @Override
  protected void checkIfIndexPainted(final Graphics2D g2d, final Transcaler ts, final int index) {
    final int oldStart = getStartIndex();
    final int oldEnd = getEndIndex();

    if(oldStart == -1) {    // first pick;
      recheckIndex(g2d, ts, index);
      setEndIndex(index);
      setStartIndex(index);
      return;
    } else if (oldStart == oldEnd) { // 2nd pick;

      if(oldStart == index) { //didn't switch wedges
        return;
      }

      int distClock, distAntiClock;
      distClock = distClock(oldStart, index);
      distAntiClock = distAntiClock(oldStart, index);
      if(distClock <= distAntiClock) {
        setEndIndex(index);
      } else {
        setStartIndex(index);
      }

      }  else { // 3rd and higher pick;

      int distAntiClockStart = distAntiClock(oldStart, index);
      int distClockEnd = distClock(oldEnd, index);

      if(distAntiClockStart == 0 || distClockEnd == 0) {
         return;
      }

      if(!isBetweenClockwise(index, oldStart, oldEnd)) { //if not already picked

        if (distAntiClockStart <= distClockEnd) {
          setStartIndex(index);
        } else {
          setEndIndex(index);
        }
      }
    }

    updateIndexes(g2d, ts);

  }

  private void updateIndexes(Graphics2D g2d, Transcaler ts) {
    final int newStart = getStartIndex();
    final int newEnd = getEndIndex();

    if(newEnd <= newStart) {
      for (int i = newStart; i >= newEnd; i--) {
        recheckIndex(g2d, ts, i);
      }
    } else {

      for (int i = newStart; i >= 0; i--) {
        recheckIndex(g2d, ts, i);
      }
      for (int i = getN()-1; i >= newEnd; i--) {
        recheckIndex(g2d, ts, i);
      }
    }
  }
}
