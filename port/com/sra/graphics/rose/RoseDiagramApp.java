/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose;


import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import com.lgc.prodesk.navigator.Navigator;
import com.lgc.prowess.jms.MsgClient;
import com.lgc.prowess.jms.MsgClientException;
import edu.mines.jtk.mosaic.PlotFrame;
import org.javaseis.io.Seisio;

/**
 * Main driver for Rose diagram app.
 * @author Steve Angelovich
 * @author Derek Parks
 *
 */
public class RoseDiagramApp {
  private static final long serialVersionUID = 1L;
  public static void main(String[] args) {
    Navigator.initLookAndFeel();

    if (args.length != 1) {
      JOptionPane.showMessageDialog(null, "Please Select a shot organized dataset.");
      throw new IllegalArgumentException("Please Select a shot organized dataset.");
    }

    if(!Seisio.isJavaSeis(args[0])) {
      JOptionPane.showMessageDialog(null, "Rose diagram only supports JavaSeis datasets.");
      throw new IllegalArgumentException("Rose diagram only supports JavaSeis datasets.");
    }

    RoseDiagramPanel p = new RoseDiagramPanel(args[0]);

    PlotFrame frame = new PlotFrame(p);
    JFrame controlFrame = new JFrame();
    controlFrame.setLocationRelativeTo(frame);
    VolumeSelectorPanel vsp = new VolumeSelectorPanel(p);
    controlFrame.add(vsp);
    new RoseDiagramMenuToolBar(frame, controlFrame, p, vsp);
    frame.pack();
    frame.setVisible(true);
    frame.setSize(800, 800);
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


    try {
      MsgClient.getSharedInstance().addLocationEventListener(p);
      MsgClient.getSharedInstance().addLocationEventListener(vsp);
    } catch (MsgClientException e) {
      throw (new RuntimeException(e.toString()));
    }

    URL iconURL = RoseDiagramApp.class.getResource("favicon.png");

    // iconURL is null when not found
    ImageIcon icon = new ImageIcon(iconURL);
    Image image = icon.getImage();
    frame.setIconImage(image);

    controlFrame.pack();
    controlFrame.setVisible(true);
    controlFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    controlFrame.setIconImage(image);
  }
}
