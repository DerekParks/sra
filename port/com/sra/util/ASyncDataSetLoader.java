/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.util;


import java.io.IOException;
import java.nio.channels.ClosedByInterruptException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * A small class to handle asynchronous reading of data sets.
 * 
 * @author Derek Parks
 */
public class ASyncDataSetLoader {
  private static final Logger LOG = Logger.getLogger(ASyncDataSetLoader.class.getName());
  private final ExecutorService _executorService = Executors.newSingleThreadExecutor();
  private Future<?> lastLoad = null;
  private final AtomicLong lastLoadVKey = new AtomicLong();
  private final AtomicLong lastLoadHKey = new AtomicLong();
  private final AtomicLong lastLoadFKey = new AtomicLong();
  private final String _path;
  private final DataReady _dataReady;
  public interface DataReady {
    public void onDataReady(final ReadJSGather.GatherData data);
  }

  public ASyncDataSetLoader(final String path, DataReady dataReady) {
    _path = path;
    _dataReady = dataReady;
  }

  public void switchViewToNewLoc(final boolean shotPerVolume, final long hKey, final long vKey, final long fKey) {

    /**
     * The call to read a dataset will be done a asynchronously.
     * So this will return quick but we need to ensure that no other calls mess with lastLoad with we are.
     */
    synchronized (this) {

      if (lastLoad != null && !lastLoad.isDone()) { //if we aren't done loading from the previous click
        lastLoad.cancel(true); //stop loading the dataset
      }
      lastLoadVKey.set(vKey);
      lastLoadHKey.set(hKey);
      lastLoadFKey.set(fKey);
      lastLoad = asyncReadDataset(shotPerVolume, hKey, vKey, fKey);

    }
  }

  public long getLastLoadVKey() {
    return lastLoadVKey.get();
  }

  public long getLastLoadHKey() {
      return lastLoadHKey.get();
    }

  public long getLastLoadFKey() {
        return lastLoadFKey.get();
      }


  private Future<?> asyncReadDataset(final boolean shotPerVolume, final long hKey, final long vKey, final long fKey) {
    return _executorService.submit(new DataLoaderRunable(shotPerVolume, hKey, vKey, fKey));
  }

  private final class DataLoaderRunable implements Runnable {
    private final long _vKey;
    private final long _hKey;
    private final long _fKey;
    private final boolean _shotPerVolume;
    private DataLoaderRunable(final boolean shotPerVolume, final long hKey, final long vKey, final long fKey) {
      _vKey = vKey;
      _hKey = hKey;
      _fKey = fKey;
      _shotPerVolume = shotPerVolume;
    }

    @Override
    public void run() {
      try (ReadJSGather reader = new ReadJSGather(_path)) {
        final ReadJSGather.GatherData data;
        if(_shotPerVolume) {
          LOG.log(Level.INFO, String.format("Reading volume [%d %d]", _hKey, _vKey));
          data = reader.readVolume(_hKey, _vKey);
        } else {
          LOG.log(Level.INFO, String.format("Reading frame [%d %d %d]", _hKey, _vKey, _fKey));
          data = reader.readGather(_hKey, _vKey, _fKey);
        }
        _dataReady.onDataReady(data);
      } catch (IOException e) {
        if (e.getCause() instanceof ClosedByInterruptException) {
          //shallow this one. This is expected if we get called to load another part of the volume while still loading the previous.
        } else {
          LOG.log(Level.SEVERE, String.format("Could not read dataset: %s", _path), e);
        }
      }
    }
  }
}
